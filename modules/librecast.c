/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 *
 * librecast.c
 *
 * this file is part of LIBRESTACK
 *
 * Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file COPYING in the distribution).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "../src/err.h"
#include "../src/handler.h"
#include "../src/log.h"
#include "../src/str.h"
#include "librecast.h"

#include <arpa/inet.h>
#include <assert.h>
#include <ifaddrs.h>
#include <inttypes.h>
#include <linux/if_packet.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <librecast.h>
#include <linux/ipv6.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>

#ifndef lc_message_head_t
typedef struct lc_message_head_t {
	uint64_t timestamp; /* nanosecond timestamp */
	lc_seq_t seq; /* sequence number */
	lc_rnd_t rnd; /* nonce */
	uint8_t op;
	lc_len_t len;
} __attribute__((__packed__)) lc_message_head_t;
#endif

/* `tcpdump -dd "ip6 multicast && udp"` */
struct sock_filter bpf_ip6multi[] = {
	{ 0x28, 0, 0, 0x0000000c },
	{ 0x15, 0, 8, 0x000086dd },
	{ 0x30, 0, 0, 0x00000026 },
	{ 0x15, 0, 6, 0x000000ff },
	{ 0x30, 0, 0, 0x00000014 },
	{ 0x15, 3, 0, 0x00000011 },
	{ 0x15, 0, 3, 0x0000002c },
	{ 0x30, 0, 0, 0x00000036 },
	{ 0x15, 0, 1, 0x00000011 },
	{ 0x6, 0, 0, 0x00040000 },
	{ 0x6, 0, 0, 0x00000000 },
};
size_t bpf_ip6multilen = sizeof bpf_ip6multi  / sizeof bpf_ip6multi[0];

typedef struct lcast_sock_t {
	lc_socket_t *sock;
	uint32_t id;
	uint32_t token;
	struct lcast_sock_t *next;
} lcast_sock_t;

typedef struct lcast_chan_t {
	lc_channel_t *chan;
	uint32_t id;
	char *name;
	struct lcast_chan_t *next;
} lcast_chan_t;

static conn_t *websock;
static pthread_t keepalive_thread;
static pthread_t tid_listen;
static lc_ctx_t *lctx;
static char ifname[IF_NAMESIZE];
static struct sockaddr_in6 llink;
static unsigned char hwaddr[ETH_ALEN];
static unsigned int ifx;
static int fdtap;
session_t session;
uint64_t uid;
uint64_t sid;
uint64_t sss;

lcast_chan_t *lcast_channel_byid(uint32_t id);
lcast_chan_t *lcast_channel_byname(char *name);
lcast_chan_t *lcast_channel_new(char *name);
lcast_sock_t *lcast_socket_byid(uint32_t id);
lcast_sock_t *lcast_socket_new(void);
int lcast_frame_send(conn_t *c, lcast_frame_t *req, char *payload, size_t paylen);
void lcast_recv(lc_message_t *msg);
void lcast_recv_err(int err);

static int lcast_session_id(uint64_t *sid)
{
	FILE *fd = fopen("/dev/urandom", "r");
	if (fd == NULL)
		return -1;
	if (fread(sid, sizeof *sid, 1, fd) != sizeof *sid)
		return -1;
	fclose(fd);
	return 0;
}

static void lcast_session_start(void)
{
	lcast_session_id(&sid);
	sss = time(NULL);
	memset(&session, 0, sizeof session);
#ifdef LCDB
	lcast_session_register();
#endif
}

static void lcast_session_update(uint64_t byi, uint64_t byo, uint64_t wsi, uint64_t wso)
{
	if (session.byi + byi > UINT64_MAX || session.byo + byo > UINT64_MAX
	||  session.wsi + wsi > UINT64_MAX || session.wso + wso > UINT64_MAX)
	{
		/* overflow, start new session */
		lcast_session_update(0, 0, 0, 0);
		lcast_session_start();
	}
	session.end = time(NULL);
	session.byi += byi;
	session.byo += byo;
	logmsg(LOG_DEBUG, "session %lu bytes in %lu bytes out", session.byi, session.byo);
	/* TODO: configure option - log to local db and/or channel */
}

static int lcast_cmd_register(conn_t *c, lcast_frame_t *req, char *payload, size_t len)
{
	(void)c, (void)req, (void)payload, (void)len; /* FIXME */
	logmsg(LOG_TRACE, "%s", __func__);
	/* TODO: unpack / check sig on cap token */
	/* TODO: set uid */
	return 0;
}

int lcast_frame_send(conn_t *c, lcast_frame_t *req, char *payload, size_t paylen)
{
	char buf[sizeof(lcast_frame_t) + paylen];
	size_t len_head;
	size_t len_body;
	size_t len_send;
	ssize_t bytes;

	logmsg(LOG_TRACE, "%s", __func__);
	assert(req);
	len_head = sizeof(lcast_frame_t);
	len_body = (size_t)paylen;
	len_send = len_head + len_body;

	//lcast_cmd_debug(req, payload);

	memcpy(buf, req, sizeof(lcast_frame_t));
	if (payload && paylen > 0) {
		memcpy(buf + len_head, payload, len_body);
	}
	DEBUG("lcast_frame_send sending %zi bytes (head)", len_head);
	DEBUG("lcast_frame_send sending %zi bytes (body)", len_body);
	DEBUG("lcast_frame_send sending %zi bytes (total)", len_send);

	if ((bytes = ws_send(c, WS_OPCODE_BINARY, buf, len_send)) > 0)
		lcast_session_update(0, 0, 0, bytes);

	return 0;
}

void lcast_cmd_debug(lcast_frame_t *req, char *payload)
{
	(void)payload;
	char *command = lcast_cmd_name(req->opcode);

	logmsg(LOG_TRACE, "%s", __func__);
	DEBUG("(librecast) %s: opcode='%x'", command, req->opcode);
#if 0
	DEBUG("(librecast) %s: len='%u'", command, req->len);
	DEBUG("(librecast) %s: id='%u'", command, req->id);
	DEBUG("(librecast) %s: id2='%u'", command, req->id2);
	DEBUG("(librecast) %s: token='%u'", command, req->token);
#endif
#ifdef LCAST_DEBUG_LOG_PAYLOAD
	if (payload) {
		char *msg = calloc(1, req->len + 1);
		memcpy(msg, payload, req->len);
		DEBUG("(librecast) %s: '%s'", command, msg);
		free(msg);
	}
#endif
	logmsg(LOG_FULLTRACE, "%s exiting", __func__);
}

int lcast_cmd_noop(conn_t *c, lcast_frame_t *req, char *payload, size_t len)
{
	(void)c, (void)req, (void)payload, (void)len;
	logmsg(LOG_TRACE, "%s", __func__);
	return 0;
}

int lcast_cmd_handler(conn_t *c, ws_frame_t *f)
{
	static char *stash = NULL;
	char *payload = NULL;
	static uint64_t len = 0;
	char *data = (char *)(f->data) + sizeof(lcast_frame_t);
	lcast_frame_t *req = (lcast_frame_t *)f->data;

	logmsg(LOG_TRACE, "%s", __func__);
#if 0
	lcast_session_update(0, 0, req.len, 0);
#endif

	// FIXME - continuation must be handled in websocket.c
	if (f->opcode <= 0x2) {
		/* data frame */
		if (f->opcode != WS_OPCODE_CONTINUE) {
			/* first or only frame in set */
			len = 0;
			free(stash);
			stash = NULL;
		}
		stash = realloc(stash, f->len + len);
		assert(stash);
		memcpy(stash + len, data, f->len);
		//lcast_cmd_debug(&req, stash);
		len += f->len;

		payload = stash;
	}

	/* NB: control frames can arrive between fragmented data frames */

	if (f->fin) {
		/* FIN bit set. This is either the last or only frame in the set. */
		switch (req->opcode) {
			LCAST_OPCODES(LCAST_OP_FUN)
		default:
			ERRMSG(LSD_ERROR_LIBRECAST_OPCODE_INVALID);
		}
		free(stash);
		stash = NULL;
	}

	return 0;
}

char *lcast_cmd_name(lcast_opcode_t opcode)
{
	logmsg(LOG_TRACE, "%s", __func__);
	LCAST_OPCODES(LCAST_OP_CODE)
	return NULL;
}

int lcast_handle_client_data(conn_t *c, ws_frame_t *f)
{
	logmsg(LOG_TRACE, "%s", __func__);
	DEBUG("lc_handle_client_data() has opcode 0x%x", f->opcode);

	switch (f->opcode) {
	case 0x0:
		DEBUG("(librecast) DATA (continuation frame)");
		return lcast_cmd_handler(c, f);
	case 0x1:
		DEBUG("(librecast) DATA (text)");
		FAIL(LSD_ERROR_NOT_IMPLEMENTED);
	case 0x2:
		DEBUG("(librecast) DATA (binary)");
		return lcast_cmd_handler(c, f);
	default:
		DEBUG("opcode 0x%x not valid for data frame", f->opcode);
		break;
	}

	return 0;
}

int lcast_cmd_ping(conn_t *c, lcast_frame_t *req, char *payload, size_t len)
{
	(void)payload, (void)len;
	DEBUG("%s", __func__);
	DEBUG("payload length = %zu", len);
	if (!fdtap) lcast_init(c);
	req->opcode = LCAST_OP_PONG;
	/* NB: the hoplimit field is being abused as a sequence number here */
	lcast_frame_send(c, req, NULL, 0);
	return 0;
}

void lcast_read_header(char *payload, size_t len)
{
	lcast_tlv_t *tlv = (lcast_tlv_t *)payload;
	DEBUG("payload length = %zu", len);
	DEBUG("TLV (type) = %u", tlv->type);
	DEBUG("TLV (len) = %u", ntohl(tlv->len));
}

uint8_t * lcast_read_header_dest(char *payload)
{
	lcast_tlv_t *tlv = (lcast_tlv_t *)payload;
	while (tlv && tlv->type) {
		if (tlv->type == 1) return tlv->value;
		tlv += 2 + ntohl(tlv->len);
	}
	return NULL;
}

int lcast_cmd_send(conn_t *c, lcast_frame_t *req, char *payload, size_t len)
{
	(void)c;
	struct ethhdr eth = {0};
	struct ipv6hdr v6h = {0};
	struct udphdr udph = {0};
	struct iovec iov[5];
	uint8_t *dst;
	int iovc = (int)(sizeof iov / sizeof iov[0]);
	ssize_t byt;
	DEBUG("%s", __func__);

	DEBUG("hop limit: %u", req->hoplimit);
	if (req->hoplimit < 2) return 0;
	req->hoplimit--;

	if (!fdtap) lcast_init(c);

	lcast_read_header(payload, len);
	dst = lcast_read_header_dest(payload);

	iov[0].iov_base = &eth;
	iov[0].iov_len = sizeof eth;
	iov[1].iov_base = &v6h;
	iov[1].iov_len = sizeof v6h;
	iov[2].iov_base = &udph;
	iov[2].iov_len = sizeof udph;
	iov[3].iov_base = req;
	iov[3].iov_len = sizeof (lcast_frame_t);
	iov[4].iov_base = payload;
	iov[4].iov_len = len - sizeof (lcast_frame_t);

	DEBUG("eth %zu bytes", iov[0].iov_len);
	DEBUG("iv6 %zu bytes", iov[1].iov_len);
	DEBUG("udp %zu bytes", iov[2].iov_len);
	DEBUG("lca %zu bytes", iov[3].iov_len);
	DEBUG("pay %zu bytes", iov[4].iov_len);

	udph.source = htons(LC_DEFAULT_PORT);
	udph.dest = udph.source;
	udph.len = htons(len + sizeof udph); /* UDP length includes the UDP header (RFC 768) */
	udph.check = 0xffff;

	v6h.version = 6;
	v6h.payload_len = udph.len;
	v6h.nexthdr = 17; /* UDP */
	v6h.hop_limit = req->hoplimit;
	memcpy(&v6h.saddr, &llink.sin6_addr, sizeof(struct in6_addr));
	v6h.daddr.s6_addr[0] = 0xff;
	v6h.daddr.s6_addr[1] = 0x1e;

	memcpy(&v6h.daddr.s6_addr[2], dst, 14);

	eth.h_dest[0] = 0x33;
	eth.h_dest[1] = 0x33;
	/* lowest 32 bits of multicast address */
	memcpy(&eth.h_dest[2], &v6h.daddr.s6_addr[12], 4);
	memcpy(eth.h_source, hwaddr, ETH_ALEN);
	eth.h_proto = htons(ETH_P_IPV6);

	byt = writev(fdtap, iov, iovc);
	if (byt == -1) {
		ERROR("writev: %s", strerror(errno));
		return -1;
	}
	DEBUG("%s(): %zi bytes sent", __func__, byt);
	return 0;
}

int lcast_cmd_join(conn_t *c, lcast_frame_t *req, char *payload, size_t len)
{
	(void)c, (void)req, (void)payload, (void)len;
	DEBUG("%s", __func__);
	DEBUG("payload length = %zu", len);
	return 0;
}

int lcast_cmd_part(conn_t *c, lcast_frame_t *req, char *payload, size_t len)
{
	(void)c, (void)req, (void)payload, (void)len;
	DEBUG("%s", __func__);
	return 0;
}

static void * lcast_keepalive(void *arg)
{
	(void)arg;
	unsigned int seconds = LCAST_KEEPALIVE_INTERVAL;
	ssize_t bytes;

	while(websock) {
		sleep(seconds);
		DEBUG("keepalive ping (%us)", seconds);
		if ((bytes = ws_send(websock, WS_OPCODE_PING, NULL, 0)) < 2)
			break;
		lcast_session_update(0, 0, 0, bytes);
	}
	DEBUG("thread %s exiting", __func__);

	return NULL;
}

static int process_msg(conn_t *c, char *buf, size_t sz)
{
	struct ip6_hdr *ip6h = (struct ip6_hdr *)buf;
	char *ptr = ((char *)ip6h) + (ssize_t)sizeof(struct ip6_hdr);
	for (uint8_t nxt = ip6h->ip6_nxt; nxt != 255; ) {
		switch (nxt) {
			case 0: /* Hop-by-Hop options header */
				ptr += ((struct ip6_hbh *)ptr)->ip6h_len * 8;
				nxt = ((struct ip6_hbh *)ptr)->ip6h_nxt;
				break;
			case 17: /* UDP */
				nxt = 255;
				size_t ip6len = (size_t)(ptr - buf);
				size_t udphlen = sizeof(struct udphdr);
				size_t paylen = sz - ip6len - udphlen;
				ptr += udphlen;
				ssize_t bytes;
				if ((bytes = ws_send(c, WS_OPCODE_BINARY, ptr, paylen)) > 0)
					DEBUG("%zi bytes sent", bytes);
				return 0;
			case 58: /* ICMPv6 */
				nxt = 255;
				DEBUG("ICMPv6");
				break;
			default:
				nxt = 255;
				break;
		}
	}
	return -1;
}

void *thread_listen(void *arg)
{
	conn_t *c = (conn_t *)arg;
	char buf[65535];
	struct ethhdr eth = {0};
	struct iovec iov[2];
	ssize_t byt;

	iov[0].iov_base = &eth;
	iov[0].iov_len = sizeof eth;
	iov[1].iov_base = buf;
	iov[1].iov_len = sizeof buf;

	DEBUG("%s() starting up", __func__);
	while (1) {
		byt = readv(fdtap, iov, sizeof iov / sizeof iov[0]);
		if (byt == -1) {
			ERROR("readv: %s", strerror(errno));
		}
		else if (byt > 0) {
			DEBUG("received %zi bytes", byt);
			byt -= (ssize_t)sizeof eth;
			DEBUG("ethernet payload %zi bytes (%zu eth header)", byt, sizeof eth);
			if (!process_msg(c, buf, (size_t)byt))
				DEBUG("dropping packet");
		}
	}
	return arg;
}

void lcast_init(conn_t *c)
{
	logmsg(LOG_TRACE, "%s", __func__);
	lcast_session_start();
	if (lctx == NULL) {
		lctx = lc_ctx_new();
		fdtap = lc_tap_create(ifname);
		if (fdtap == -1) {
			ERROR("lc_tap_create: %s", strerror(errno));
			_exit(EXIT_FAILURE);
		}
		DEBUG("tap '%s' created", ifname);
		lc_link_set(lctx, ifname, 1);
		ifx = if_nametoindex(ifname);
		struct ifreq ifr = {0};
		strncpy(ifr.ifr_name, ifname, sizeof ifname);
		int sock = socket(AF_INET6, SOCK_DGRAM, 0);
		if (ioctl(sock, SIOCGIFHWADDR, &ifr) == -1) {
			ERROR("ioctl(SIOCGIFHWADDR): %s", strerror(errno));
			_exit(EXIT_FAILURE);
		}
		memcpy(hwaddr, &ifr.ifr_hwaddr.sa_data, ETH_ALEN);
		close(sock);
		struct sockaddr_in6 *llocal;
		struct ifaddrs *ifaddr = NULL;
		if (getifaddrs(&ifaddr)) {
			ERROR("getifaddrs: %s", strerror(errno));
			_exit(EXIT_FAILURE);
		}
		for (struct ifaddrs *ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
			if (if_nametoindex(ifa->ifa_name) != ifx) continue;
			if (ifa->ifa_addr->sa_family !=AF_INET6) continue;
			llocal = ((struct sockaddr_in6 *)ifa->ifa_addr);
			if (!IN6_IS_ADDR_LINKLOCAL(&llocal->sin6_addr)) continue;
			if (!(ifa->ifa_flags & IFF_MULTICAST)) continue;
			memcpy(&llink, llocal, sizeof(struct sockaddr_in6));
			break;
		}
		freeifaddrs(ifaddr);
		pthread_create(&tid_listen, NULL, &thread_listen, c);
	}
	assert(lctx != NULL);

	/* TODO start listen thread */

	/* start PING thread */
	if (keepalive_thread == 0) {
		pthread_attr_t attr = {0};
		pthread_attr_init(&attr);
		pthread_create(&keepalive_thread, &attr, lcast_keepalive, NULL);
		pthread_attr_destroy(&attr);
	}
}

#if 0
void lcast_recv(lc_message_t *msg)
{
	(void)msg;
	lcast_frame_t req = {0};
	char *data;
	//size_t skip = 0;

	logmsg(LOG_TRACE, "%s", __func__);
	//lcast_session_update(msg->bytes, 0, 0, 0);
	//req.opcode = LCAST_OP_SOCKET_MSG;
	//req.len = msg->len - skip;
	//data = (char *)msg->data + skip;
	//req.id = msg->sockid;
	//req.timestamp = msg->timestamp;

	//lcast_sock_t *s;
#if 0
	if ((s = lcast_socket_byid(msg->sockid)) != NULL)
		req.token = s->token;
#endif

	lcast_frame_send(websock, &req, data, req.len);
}
#endif

void lcast_recv_err(int err)
{
	logmsg(LOG_TRACE, "%s", __func__);
	DEBUG("lcast_recv_err(): %i", err);
}
