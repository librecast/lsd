/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 * Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef __LIBRECAST_H__
#define __LIBRECAST_H__ 1

#include "websocket.h"
#include <librecast.h>
#include <librecast/if.h>
#include <stdint.h>

typedef struct lcast_frame_t {
	uint8_t opcode;
	uint8_t hoplimit;
} __attribute__((__packed__)) lcast_frame_t;

typedef struct lcast_tlv_s {
	uint8_t  type;
	uint32_t len;
	uint8_t  value[];
} __attribute__((__packed__)) lcast_tlv_t;

enum {
	LCAST_TLV_NOP = 0x0,
	LCAST_TLV_DST = 0x1,
	LCAST_TLV_SRC = 0x2
};

#define LCAST_OPCODES(X) \
	X(0x00, LCAST_OP_NOOP,           "NOOP",           lcast_cmd_noop) \
	X(0x01, LCAST_OP_PING,           "PING",           lcast_cmd_ping) \
	X(0x02, LCAST_OP_PONG,           "PONG",           lcast_cmd_noop) \
	X(0x03, LCAST_OP_SEND,           "SEND",           lcast_cmd_send) \
	X(0x04, LCAST_OP_JOIN,           "JOIN",           lcast_cmd_join) \
	X(0x05, LCAST_OP_PART,           "PART",           lcast_cmd_part) \
	X(0x06, LCAST_OP_USER,           "USER",           lcast_cmd_send) \
	X(0x07, LCAST_OP_PEER,           "PEER",           lcast_cmd_send) \
	X(0x15, LCAST_OP_REGISTER,       "REGISTER",       lcast_cmd_register)
#undef X

#define LCAST_TEXT_CMD(code, name, cmd, fun) if (strncmp(f->data, cmd, strlen(cmd))==0) return fun(sock, f, f->data + strlen(cmd));
#define LCAST_OP_CODE(code, name, cmd, fun) if (name == opcode) return cmd;
#define LCAST_OP_FUN(code, name, cmd, fun) case code: logmsg(LOG_DEBUG, "%s", cmd); fun(c, req, payload, len); break;
#define LCAST_OPCODES_ENUM(code, name, text, fun) name = code,

typedef enum {
	LCAST_OPCODES(LCAST_OPCODES_ENUM)
} lcast_opcode_t;

typedef struct session_s {
	uint64_t end;	/* session last active */
	uint64_t byi;	/* bytes in (multicast) */
	uint64_t byo;	/* bytes oot (multicast) */
	uint64_t wsi;	/* bytes in (websocket) */
	uint64_t wso;	/* bytes oot (websocket) */
} __attribute__((__packed__)) session_t;

extern session_t session;
extern uint64_t uid;	/* user id associated with this websocket */
extern uint64_t sid;	/* session id */
extern uint64_t sss;	/* session started */

#define LCAST_KEEPALIVE_INTERVAL 15

/* return cmd name from opcode */
char *lcast_cmd_name(lcast_opcode_t opcode);

/* debug logs */
void lcast_cmd_debug(lcast_frame_t *req, char *payload);

int lcast_cmd_noop(conn_t *c, lcast_frame_t *req, char *payload, size_t len);
int lcast_cmd_ping(conn_t *c, lcast_frame_t *req, char *payload, size_t len);
int lcast_cmd_send(conn_t *c, lcast_frame_t *req, char *payload, size_t len);
int lcast_cmd_join(conn_t *c, lcast_frame_t *req, char *payload, size_t len);
int lcast_cmd_part(conn_t *c, lcast_frame_t *req, char *payload, size_t len);

/* process client command */
int lcast_cmd_handler(conn_t *c, ws_frame_t *f);

/* deal with incoming client data frames */
int lcast_handle_client_data(conn_t *c, ws_frame_t *f);

/* initialize librecast context and socket */
void lcast_init();

#endif /* __LIBRECAST_H__ */
