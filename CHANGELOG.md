# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added
- CHANGELOG.md

### Fixed
- link misc.o in tests
- remove unneeded libs from LDLIBS
- fix #15 MDB_READERS_FULL - clear stale readers
- removed obsolete librecast db calls
- Gave Up Github (https://sfconservancy.org/GiveUpGitHub/). Hello Codeberg!

## [0.0.2] - 2020-09-07 Bugfix release
## [0.0.1] - 2020-09-07 Bugfix release
## [0.0.0] - 2020-09-06 Initial release
